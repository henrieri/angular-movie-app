import { Store, select } from '@ngrx/store';
import { zip } from 'rxjs';
import { Injectable } from '@angular/core';
import { Movie } from '../models/movie.model';
import { AppState } from '../app.state';
import { MoviesState } from '../reducers/movies.reducer';
import { Observable } from 'rxjs';

@Injectable()
export class MoviesQuery {
  selectMovies$ = this.store.select(state => state.movies.byId);
  selectMovieIds$ = this.store.select(state => state.movies.allIds);

  constructor(private store: Store<AppState>) {}

  getMovies() {
    return this._getMovies(this.selectMovieIds$);
  }

  private _getMovies(selectMovieIds$: Observable<string[]>) {
    return zip(
      this.selectMovies$,
      selectMovieIds$,
      (movies: MoviesState, movieIds: Array<number>) =>
        movieIds.map(movieId => new Movie(movies[movieId]))
    );
  }

  getMovie(movieId: number) {
    return zip(
      this.selectMovies$,
      (movies: MoviesState) =>
        movies[movieId] ? new Movie(movies[movieId]) : null
    );
  }
}
