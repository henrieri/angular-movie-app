import { MoviesService } from '../services/movies.service';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TestBed } from '@angular/core/testing';
import { DatabaseService, movies } from '../services/database.service';
import { MoviesQuery } from '../queries/movies.query';
import { reducer } from '../reducers/reducer';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';

describe('MoviesQuery', () => {
  let query: MoviesQuery;
  let store: Store<AppState>;
  let service: MoviesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientInMemoryWebApiModule.forRoot(DatabaseService, { delay: 0 }),
        StoreModule.forRoot(reducer)
      ],
      providers: [MoviesService, MoviesQuery]
    });

    query = TestBed.get(MoviesQuery);
    store = TestBed.get(Store);
    service = TestBed.get(MoviesService);
    service.fetchAndStoreMovies();
  });

  it('#getMovies should return movies from store as observable', (done: DoneFn) => {
    query.getMovies().subscribe(storedMovies => {
      expect(storedMovies[0].name).toContain('Deadpool');
      done();
    });
  });

  it('#getMovie should return movie by id from store as observable', async (done: DoneFn) => {
    query.getMovie(1).subscribe(storedMovie => {
      expect(storedMovie.name).toContain('Deadpool');
      done();
    });
  });
});
