import {
  trigger,
  animate,
  transition,
  style,
  query,
  stagger
} from '@angular/animations';

export const fadeAnimation = trigger('fadeAnimation', [
  transition('* => *', [
    query(
      ':enter', [style({
        opacity: 0
      })], {
        optional: true
      }
    ),
    query(
      ':leave', [style({
        opacity: 1,
      }), animate('0.1s', style({
        opacity: 0
      }))], {
        optional: true
      }
    ),
    query(
      ':enter', [style({
        opacity: 0,
      }), animate('0.1s', style({
        opacity: 1
      }))], {
        optional: true
      }
    )
  ])
]);

export const listAnimation = trigger('listAnimation', [
  transition('0 => *', [
    query(
      ':enter', [
        style({
          opacity: 0,
          transform: 'translateY(10px)'
        }),
        stagger(150, [
          animate('0.5s', style({
            opacity: 1,
            transform: 'translateY(0)'
          }))
        ])
      ], {
        optional: true
      }
    )
  ])
])
