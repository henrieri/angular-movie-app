import { normalizeResponse } from './redux';

describe('redux', () => {
  it(`#normalizeResponse should take response
  and normalize it reducing array to an object with byId key and allIds key`, () => {
    const normalized = normalizeResponse([
      {
        id: 1,
        name: 'Test'
      },
      {
        id: 2,
        name: 'Test 2'
      }
    ]);

    expect(normalized).toEqual({
      byId: {
        1: {
          id: 1,
          name: 'Test'
        },
        2: {
          id: 2,
          name: 'Test 2'
        }
      },
      allIds: [1, 2]
    });
  });
});
