export interface NormalizedObjectsState<T> {
  byId: { [id: string]: T };
  allIds: string[];
}

export function normalizeResponse(payload: Array<any>) {
  const byId = payload.reduce(
    (normalized, object) => ({
      ...normalized,
      [object.id]: object
    }),
    {}
  );

  const allIds = payload.map(object => object.id);

  return { byId, allIds };
}
