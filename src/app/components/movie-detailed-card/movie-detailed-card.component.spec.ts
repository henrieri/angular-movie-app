import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieDetailedCardComponent } from './movie-detailed-card.component';
import { RouterTestingModule } from '@angular/router/testing';
import { testMovie } from '../../utils/testing/data';

describe('MovieDetailedCardComponent', () => {
  let component: MovieDetailedCardComponent;
  let fixture: ComponentFixture<MovieDetailedCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MovieDetailedCardComponent],
      imports: [RouterTestingModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailedCardComponent);
    component = fixture.componentInstance;
    component.movie = testMovie;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
