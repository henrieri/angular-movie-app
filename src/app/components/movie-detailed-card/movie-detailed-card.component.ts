import { Component, OnInit } from '@angular/core';
import { Movie } from '../../models/movie.model';
import { Input } from '@angular/core';

@Component({
  selector: 'app-movie-detailed-card',
  templateUrl: './movie-detailed-card.component.html',
  styleUrls: ['./movie-detailed-card.component.scss']
})
export class MovieDetailedCardComponent implements OnInit {
  @Input()
  movie: Movie;

  constructor() {}

  ngOnInit() {}
}
