import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { MovieCardComponent } from "./movie-card.component";
import { genreType } from "../../models/genre.model";
import { Movie } from "../../models/movie.model";
import { movies } from "../../services/database.service";
import { RouterTestingModule } from "@angular/router/testing";
import { testMovie } from "../../utils/testing/data";

describe("MovieCardComponent", () => {
  let component: MovieCardComponent;
  let fixture: ComponentFixture<MovieCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MovieCardComponent],
      imports: [RouterTestingModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieCardComponent);
    component = fixture.componentInstance;
    component.movie = testMovie;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
