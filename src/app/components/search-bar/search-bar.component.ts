import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  public searchForm: FormGroup;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      query: this.activatedRoute.snapshot.queryParams.query
    });

    this.searchForm.valueChanges.subscribe(valueChange => {
      this.setQueryString(valueChange.query);
    });
  }
  public setQueryString(query) {
    this.router.navigate(['.'], {
      queryParams: {
        ...this.activatedRoute.snapshot.queryParams,
        query
      },
      replaceUrl: true
    });
  }
}
