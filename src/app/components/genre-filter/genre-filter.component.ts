import { Component, OnInit } from '@angular/core';
import { GenreType, genreType } from '../../models/genre.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-genre-filter',
  templateUrl: './genre-filter.component.html',
  styleUrls: ['./genre-filter.component.scss']
})
export class GenreFilterComponent implements OnInit {
  genres: GenreType[];
  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.genres = Object.values(genreType);
  }

  get selectedGenres() {
    const genres = this.activatedRoute.snapshot.queryParams.genres;

    return genres ? (genres instanceof Array ? genres : [genres]) : [];
  }

  genreQueryParam(genre) {
    if (this.selectedGenres.includes(genre)) {
      return {
        genres: this.selectedGenres.filter(
          selectedGenre => selectedGenre !== genre
        )
      };
    }
    return { genres: [...this.selectedGenres, genre] };
  }
}
