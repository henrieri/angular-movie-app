import { Action } from '@ngrx/store';
import { Movie } from './../models/movie.model';

export const SET_MOVIES = '[MOVIE] Set';
export interface MoviesPayload {
  movies: Movie[];
}

export class SetMovies implements Action {
  readonly type = SET_MOVIES;

  constructor(public payload: MoviesPayload) {}
}

export type Actions = SetMovies;
