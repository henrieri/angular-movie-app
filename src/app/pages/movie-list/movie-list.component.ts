import { Component, OnInit, OnDestroy } from '@angular/core';
import { Movie } from '../../models/movie.model';
import { Observable } from 'rxjs';
import { MoviesQuery } from '../../queries/movies.query';
import { MoviesService } from '../../services/movies.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { listAnimation } from '../../animations/animations';
@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
  animations: [listAnimation]
})
export class MovieListComponent implements OnInit, OnDestroy {
  movies$: Observable<Movie[]>;
  movies: Movie[] = [];
  filteredMovies: Movie[] = [];
  routerSubscription: any;
  movieSubscription: any;

  constructor(
    private moviesQuery: MoviesQuery,
    private moviesService: MoviesService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.filteredMovies = [];
    this.moviesService.fetchAndStoreMovies();
    this.movies$ = this.moviesQuery.getMovies();

    this.movieSubscription = this.movies$.subscribe(movies => {
      this.movies = movies;

      this.filterMovies();
    });

    this.routerSubscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.filterMovies();
      }
    });
  }

  filterMovies() {
    this.filteredMovies = [];

    const movies = this.movies;
    const searchQuery = this.activatedRoute.snapshot.queryParams.query;
    const genres = this.activatedRoute.snapshot.queryParams.genres;

    const searchQueryFilter = movie =>
      searchQuery
        ? movie.name.toLowerCase().includes(searchQuery.toLowerCase())
        : true;

    const genreTypeFilter = movie =>
      genres && genres.length > 0
        ? movie.genres.some(genre => genres.includes(genre))
        : true;

    const filteredMovies = movies
      .filter(searchQueryFilter)
      .filter(genreTypeFilter);

    this.filteredMovies = filteredMovies;
  }

  ngOnDestroy() {
    this.routerSubscription.unsubscribe();
    this.movieSubscription.unsubscribe();
  }
}
