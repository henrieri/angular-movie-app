import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input } from '@angular/core';
import { MovieListComponent } from './movie-list.component';
import { MoviesQuery } from '../../queries/movies.query';
import { MoviesService } from '../../services/movies.service';
import { StoreModule } from '@ngrx/store';
import { moviesReducer } from '../../reducers/movies.reducer';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DatabaseService } from '../../services/database.service';
import { Movie } from '../../models/movie.model';
import { SearchBarComponent } from '../../components/search-bar/search-bar.component';
import { GenreFilterComponent } from '../../components/genre-filter/genre-filter.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MovieCardComponent } from '../../components/movie-card/movie-card.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('MovieListComponent', () => {
  let component: MovieListComponent;
  let fixture: ComponentFixture<MovieListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MovieListComponent,
        MovieCardComponent,
        SearchBarComponent,
        GenreFilterComponent
      ],
      imports: [
        NoopAnimationsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({ movies: moviesReducer }),
        RouterTestingModule,
        HttpClientModule,
        HttpClientInMemoryWebApiModule.forRoot(DatabaseService, { delay: 0 })
      ],
      providers: [MoviesQuery, MoviesService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display list of movies', async (done: DoneFn) => {
    await component.ngOnInit();
    fixture.detectChanges();
    const htmlContainer: HTMLElement = fixture.nativeElement;

    const movieListContainer: HTMLElement = htmlContainer.querySelector(
      '.movie-card-list'
    );

    expect(movieListContainer.textContent).toContain('Deadpool');
    expect(movieListContainer.textContent).toContain('Straight Outta Compton');

    done();
  });
});
