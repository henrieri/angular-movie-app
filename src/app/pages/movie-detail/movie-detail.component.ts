import { Component, OnInit, OnDestroy } from '@angular/core';
import { MoviesQuery } from '../../queries/movies.query';
import { MoviesService } from '../../services/movies.service';
import { ActivatedRoute } from '@angular/router';
import { Movie } from '../../models/movie.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit, OnDestroy {
  movie$: Observable<Movie>;
  private routeSubscription: any;

  constructor(
    private moviesQuery: MoviesQuery,
    private moviesService: MoviesService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.moviesService.fetchAndStoreMovies();
    this.routeSubscription = this.route.params.subscribe(params => {
      this.movie$ = this.moviesQuery.getMovie(+params['id']);
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }
}
