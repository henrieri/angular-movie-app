import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { MovieDetailComponent } from './movie-detail.component';
import { SecondaryButtonComponent } from '../../components/buttons/secondary-button/secondary-button.component';
import { MovieDetailedCardComponent } from '../../components/movie-detailed-card/movie-detailed-card.component';

import { MoviesService } from '../../services/movies.service';
import { MoviesQuery } from '../../queries/movies.query';
import { StoreModule } from '@ngrx/store';
import { reducer } from '../../reducers/reducer';
import { DatabaseService } from '../../services/database.service';
import { from } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

describe('MovieDetailComponent', () => {
  let component: MovieDetailComponent;
  let fixture: ComponentFixture<MovieDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MovieDetailComponent,
        SecondaryButtonComponent,
        MovieDetailedCardComponent
      ],
      imports: [
        RouterTestingModule,
        StoreModule.forRoot(reducer),
        HttpClientModule,
        HttpClientInMemoryWebApiModule.forRoot(DatabaseService, { delay: 0 })
      ],

      providers: [
        MoviesService,
        MoviesQuery,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{ id: 1 }])
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain information about the movie', async (done: DoneFn) => {
    await component.ngOnInit();
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('h1').textContent).toContain(
      'Deadpool'
    );
    done();
  });
});
