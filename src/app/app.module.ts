import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MovieDetailComponent } from './pages/movie-detail/movie-detail.component';
import { MovieListComponent } from './pages/movie-list/movie-list.component';
import { DatabaseService } from './services/database.service';
import { MoviesService } from './services/movies.service';
import { MoviesQuery } from './queries/movies.query';
import { MovieCardComponent } from './components/movie-card/movie-card.component';
import { MovieDetailedCardComponent } from './components/movie-detailed-card/movie-detailed-card.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { SecondaryButtonComponent } from './components/buttons/secondary-button/secondary-button.component';
import { ReactiveFormsModule } from '@angular/forms';
import { GenreFilterComponent } from './components/genre-filter/genre-filter.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { reducer } from './reducers/reducer';
@NgModule({
  declarations: [
    AppComponent,
    MovieDetailComponent,
    MovieListComponent,
    MovieCardComponent,
    MovieDetailedCardComponent,
    SearchBarComponent,
    SecondaryButtonComponent,
    GenreFilterComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    HttpClientInMemoryWebApiModule.forRoot(DatabaseService, { delay: 50 }),
    StoreModule.forRoot(reducer),
    StoreDevtoolsModule.instrument({
      maxAge: 25 // Retains last 25 states
    })
  ],
  providers: [MoviesService, MoviesQuery],
  bootstrap: [AppComponent]
})
export class AppModule {}
