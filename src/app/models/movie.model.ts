import { GenreType } from './genre.model';
import { Model } from './model';

export class Movie extends Model {
  id: number;
  key: string;
  name: string;
  description: string;
  genres: Array<GenreType>;
  rate: number;
  length: number;
  img: string;

  get imageUrl() {
    return `/assets/images/movie-covers/${this.img}`;
  }

  get imageAlt() {
    return `Movie ${this.name} cover`;
  }

  get movieUrl() {
    return `/movies/${this.id}`;
  }

  get rating() {
    return this.rate;
  }

  get duration() {
    return this.length;
  }
}
