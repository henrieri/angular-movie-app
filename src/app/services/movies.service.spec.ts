import { MoviesService } from './movies.service';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TestBed } from '@angular/core/testing';
import { DatabaseService, movies } from './database.service';
import { MoviesQuery } from '../queries/movies.query';
import { reducer } from '../reducers/reducer';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import { normalizeResponse } from '../core/redux';

describe('MovieService', () => {
  let service: MoviesService;
  let store: Store<AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientInMemoryWebApiModule.forRoot(DatabaseService, { delay: 0 }),
        StoreModule.forRoot(reducer)
      ],
      providers: [MoviesService, MoviesQuery]
    });

    service = TestBed.get(MoviesService);
    store = TestBed.get(Store);
  });

  it('#fetchMovies should return movies from observable', (done: DoneFn) => {
    service.fetchMovies().subscribe(value => {
      expect(JSON.stringify(value)).toBe(JSON.stringify(movies));
      done();
    });
  });

  it('#fetchAndStoreMovies should fetch and save movies in the store', async (done: DoneFn) => {
    await service.fetchAndStoreMovies();

    store.select(state => state.movies).subscribe(moviesState => {
      expect(JSON.stringify(moviesState).slice(0, 50)).toBe(
        JSON.stringify(normalizeResponse(movies)).slice(0, 50)
      );

      expect(JSON.stringify(moviesState)).toContain('Deadpool');

      done();
    });
  });
});
