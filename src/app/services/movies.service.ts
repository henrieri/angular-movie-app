import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store, Action } from '@ngrx/store';
import { SET_MOVIES } from '../actions/movie.actions';
import { AppState } from '../app.state';

@Injectable()
export class MoviesService {
  constructor(private http: HttpClient, private store: Store<AppState>) {}

  fetchMovies() {
    return this.http.get('/app/movies');
  }

  fetchAndStoreMovies() {
    return this.fetchMovies().subscribe(movies =>
      this.store.dispatch({
        type: SET_MOVIES,
        payload: {
          movies
        }
      })
    );
  }
}
