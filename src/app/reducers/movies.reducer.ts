import * as movie from '../actions/movie.actions';
import { Movie } from '../models/movie.model';
import { NormalizedObjectsState, normalizeResponse } from '../core/redux';

export interface MoviesState extends NormalizedObjectsState<Movie> {
  fetched: Boolean;
}

const initialState: MoviesState = {
  byId: {},
  allIds: [],
  fetched: false
};

export function moviesReducer(
  state: MoviesState = initialState,
  action: movie.SetMovies
) {
  switch (action.type) {
    case movie.SET_MOVIES:
      return {
        ...state,
        ...normalizeResponse(action.payload.movies),
        fetched: true
      };
    default:
      return state;
  }
}
