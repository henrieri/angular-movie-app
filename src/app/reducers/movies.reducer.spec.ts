import { moviesReducer, MoviesState } from './movies.reducer';
import { SET_MOVIES, SetMovies, MoviesPayload } from '../actions/movie.actions';
import { testMovie } from '../utils/testing/data';

describe('MoviesReducer', () => {
  it('#moviesReducer should return state with added movies', (done: DoneFn) => {
    const initialState: MoviesState = {
      byId: {},
      allIds: [],
      fetched: false
    };

    const payload: MoviesPayload = {
      movies: [testMovie]
    };

    const action: SetMovies = {
      type: SET_MOVIES,
      payload
    };

    const newState = moviesReducer(initialState, action);
    expect(SET_MOVIES).toBe('[MOVIE] Set');
    expect(newState.allIds).toContain(1);
    expect(newState.allIds.length).toEqual(1);
    expect(newState.byId[1].name).toEqual('Deadpool');

    done();
  });
});
