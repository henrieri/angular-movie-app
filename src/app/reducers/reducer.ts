import { moviesReducer } from './movies.reducer';

export const reducer = {
  movies: moviesReducer
};
